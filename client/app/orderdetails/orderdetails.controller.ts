'use strict';
(function() {

  class OrderdetailsComponent {
    constructor($scope, service: Service) {
      $scope.data = true;
      $scope.date = service.firstTuesday;
      service.orderDetails().then(function(res: any) {
        let filtered = _.filter(
            res.data, function(o: any) { return o.name !== 'wednesday'; });
        $scope.data = _.groupBy(filtered, function(n: any) {
          if (!n.User) {
            return '';
          }
          return n.User.name;
        });
      });
    }
  }

  angular.module('foodcoopApp').component('orderdetails', {
    templateUrl: 'app/orderdetails/orderdetails.html',
    controller: OrderdetailsComponent
  });

})();
