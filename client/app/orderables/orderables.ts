'use strict';

angular.module('foodcoopApp').config(function($routeProvider) {
  $routeProvider.when('/orderables/:year/:cw',
                      {template : '<orderables></orderables>'});
});
