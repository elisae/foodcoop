'use strict';
(function () {

  class AdminComponent {
    constructor($scope, $location: angular.ILocationService, service: Service) {
      $scope.getSelectedCw = service.getSelectedCw;
      $scope.$location = $location;
      $scope.isActive = function (route) {
        return route === $scope.$location.path();
      };
      $scope.sql = 
`select * from orderables limit 10;
select * from products limit 10;
select * from users limit 10;
select * from settings;`;
      $scope.query = () => {
        service.sql($scope.password, $scope.sql).then((result) => {
          $scope.sqlresult = JSON.stringify(result.data);
        }).catch(reason => {
          $scope.sqlresult = JSON.stringify(reason);
        })
      };
    }
  }

  angular.module('foodcoopApp').component('admin', {
    templateUrl: 'app/admin/admin.html',
    controller: AdminComponent
  });

})();
