'use strict';

angular.module('foodcoopApp').config(function($routeProvider) {
  $routeProvider.when('/admin/:year/:cw', {template : '<admin></admin>'});
});
