
(function(angular, undefined) {
  angular.module("foodcoopApp.constants", [])
.constant("appConfig", {"userRoles":["guest","user","admin"]});

})(angular);

