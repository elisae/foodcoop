'use strict';

angular.module('foodcoopApp').config(function($routeProvider) {
  $routeProvider.when('/orderlists/:year/:cw',
                      {template : '<orderlists></orderlists>'});
});
