'use strict';

angular.module('foodcoopApp')
    .config(function($routeProvider) {
      $routeProvider
          .when('/login', {
            templateUrl : 'app/account/login/login.html',
            controller : 'LoginController',
            controllerAs : 'vm'
          })
          .when('/', {
            templateUrl : 'app/account/login/login.html',
            controller : 'LoginController',
            controllerAs : 'vm'
          })
          .when('/logout', {
            name : 'logout',
            referrer : '/',
            template : '',
            controller : function($location, $route, Auth) {
              let referrer = $route.current.params.referrer ||
                             $route.current.referrer || '/';
              Auth.logout();
              $location.path(referrer);
            }
          });
    })
    .run(function($rootScope) {
      $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (next.name === 'logout' && current && current.originalPath &&
            !current.authenticate) {
          next.referrer = current.originalPath;
        }
      });
    });
