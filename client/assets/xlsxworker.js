importScripts('xlsx.full.min.js');

function datenum(v, date1904 = null) {
  if (date1904) v += 1462;
  var epoch = Date.parse(v);
  return (epoch - new Date(Date.UTC(1899, 11, 30)).getUTCMilliseconds()) / (24 * 60 * 60 * 1000);
}

function sheet_from_array_of_arrays(data, opts = undefined) {
  var ws = {};
  var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
  for (var R = 0; R != data.length; ++R) {
    for (var C = 0; C != data[R].length; ++C) {
      if (range.s.r > R) range.s.r = R;
      if (range.s.c > C) range.s.c = C;
      if (range.e.r < R) range.e.r = R;
      if (range.e.c < C) range.e.c = C;
      var cell = { v: data[R][C].value, c: data[R][C].comment ? [{ a: 'None', t: data[R][C].comment }] : [] };
      if (cell.v == null) continue;
      var cellRef = self.XLSX.utils.encode_cell({ c: C, r: R });

      if (typeof cell.v === 'number') cell.t = 'n';
      else if (typeof cell.v === 'boolean') cell.t = 'b';
      else if (cell.v instanceof Date) {
        cell.t = 'n'; cell.z = self.XLSX.SSF._table[14];
        cell.v = datenum(cell.v);
      }
      else cell.t = 's';

      ws[cellRef] = cell;
    }
  }
  if (range.s.c < 10000000) ws['!ref'] = self.XLSX.utils.encode_range(range);
  return ws;
}

onmessage = function (messageEvent) {
    const data = JSON.parse(messageEvent.data);
    var wb = {
        SheetNames: [],
        Sheets: {}
    };

    Object.keys(data).forEach(function (key, column_index) {
        wb.SheetNames.push(key);
        let ws = data[key];
        for (let row_index = 0; row_index < ws.length; row_index++) {
            let row = ws[row_index];
            for (let column_index = 0; column_index < row.length; column_index++) {
                let cell = row[column_index];
                if (typeof cell !== typeof {}) {
                    row[column_index] = { value: cell }
                }
            }
        }

        wb.Sheets[key] = sheet_from_array_of_arrays(data[key]);
    });

    var wbout = self.XLSX.write(wb, { bookType: 'xlsx', compression: true, bookSST: true, type: 'binary' });

    function sheet2arraybuffer(sheet) {
        var buf = new ArrayBuffer(sheet.length);
        var view = new Uint8Array(buf);
        for (var i = 0; i != sheet.length; ++i) view[i] = sheet.charCodeAt(i) & 0xFF;
        return buf;
    }
    const result = sheet2arraybuffer(wbout);
    postMessage(result, [result]);
};
