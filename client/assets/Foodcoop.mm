<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1480861995628" ID="ID_951951519" MODIFIED="1480876144500" TEXT="Foodcoop">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<hook NAME="accessories/plugins/HierarchicalIcons.properties"/>
<node COLOR="#0033ff" CREATED="1480862180210" ID="ID_1504130690" MODIFIED="1480862262562" POSITION="left" TEXT="Brotliste">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1480862447463" ID="ID_1259325475" MODIFIED="1480862451407" POSITION="left" TEXT="Erzeugerkontakte">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1480862396122" ID="ID_397921761" MODIFIED="1480876058789" POSITION="left" TEXT="Events">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862583313" ID="ID_1171002329" MODIFIED="1480862584836" TEXT="Tag d. offenen T&#xfc;r">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862586151" ID="ID_1446328244" MODIFIED="1480862592652" TEXT="Weihnachtsfeier">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862594633" ID="ID_511882624" MODIFIED="1480862605656" TEXT="Sommerfest">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480862058955" ID="ID_59315947" MODIFIED="1480876058792" POSITION="left" TEXT="Gem&#xfc;sedienst">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862523899" ID="ID_32420493" MODIFIED="1480862696081" TEXT="Dienstag von Petrik Gem&#xfc;se holen">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480862204296" ID="ID_1924357783" MODIFIED="1480862207574" POSITION="right" TEXT="Gem&#xfc;seliste">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1480862146464" ID="ID_190030879" MODIFIED="1480876058794" POSITION="left" TEXT="Gro&#xdf;handel">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862536360" ID="ID_1098074223" MODIFIED="1481801632977" TEXT="Trockenwaren bei Viva-Naturkost">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480862013937" ID="ID_254760314" MODIFIED="1480876058796" POSITION="right" TEXT="Inventur">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862312426" ID="ID_453489749" MODIFIED="1480862318809" TEXT="1x pro Halbjahr">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862322012" ID="ID_1927735616" MODIFIED="1480862328662" TEXT="Bestandsaufnahme">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480862220064" ID="ID_1531689414" MODIFIED="1480876058798" POSITION="left" TEXT="Kleinhandel">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862190852" ID="ID_403060325" MODIFIED="1480862242073" TEXT="Honig">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862109089" ID="ID_1347686141" MODIFIED="1480862248327" TEXT="K&#xe4;se">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862187669" ID="ID_1452984520" MODIFIED="1480862253195" TEXT="Wein">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862442228" ID="ID_399094639" MODIFIED="1480862444777" TEXT="Kaffee">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480862096734" ID="ID_1797484342" MODIFIED="1480862099880" POSITION="right" TEXT="Kontof&#xfc;hrung">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1480862028227" ID="ID_454192386" MODIFIED="1480876058799" POSITION="left" TEXT="Ladendienst">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862515025" ID="ID_1747827236" MODIFIED="1480862520729" TEXT="Dienstag und Mittwoch">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480862114753" ID="ID_966912173" MODIFIED="1480876058800" POSITION="right" TEXT="Mitgliederverwaltung">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862350945" ID="ID_1360192596" MODIFIED="1480862353855" TEXT="Mailverteiler">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862355662" ID="ID_980385360" MODIFIED="1480862357422" TEXT="Liste">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480862272653" ID="ID_1827774693" MODIFIED="1480876058802" POSITION="left" TEXT="Onlinedienst">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862279285" ID="ID_1931966886" MODIFIED="1480862735699" TEXT="Korrektur der Preise">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862288698" ID="ID_116484943" MODIFIED="1480862298311" TEXT="Zettel drucken">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862299813" ID="ID_1891080151" MODIFIED="1480862306931" TEXT="Onlinedienst verwalten">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1480862385751" ID="ID_620208169" MODIFIED="1480862391366" POSITION="right" TEXT="Schl&#xfc;sselverwaltung">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1480862164905" ID="ID_1757907552" MODIFIED="1480862569385" POSITION="right" TEXT="Vermietung">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1480862416226" ID="ID_389851239" MODIFIED="1480862420942" POSITION="right" TEXT="Vollversammlungen">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1480862034422" ID="ID_225955786" MODIFIED="1480876058803" POSITION="right" TEXT="&#xd6;ffentlichkeitsarbeit">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480862330927" ID="ID_209964658" MODIFIED="1480862340712" TEXT="Facebook">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1480862343280" ID="ID_1435679804" MODIFIED="1480862348512" TEXT="Homepage">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
</node>
</map>
