#!/usr/bin/env node

// Usage:
// ./password.util.js password
// Gives: Salted Password

const _crypto = require('crypto');

const _crypto2 = _interopRequireDefault(_crypto);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

const args = process.argv.slice(2);
const password = args[0];

const salt = args[1] || _crypto2.default.randomBytes(16).toString('base64');
const hash = _crypto2.default.pbkdf2Sync(password, new Buffer(salt, 'base64'), 10000, 64, 'sha1').toString('base64');

console.log(`Password: ${password}`);

console.log(`Insert Into Users (name, password, salt, provider)`
 + `values ('Name','${hash}','${salt}','local')`);