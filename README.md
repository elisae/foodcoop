# dev
- gulp serve
- rhc scp foodcoop download /tmp/ app-root/data/dist.sqlite && cp /tmp/dist.sqlite /home/pi/backups/`date -I`.sqlite
- curl -u USER:PASSWORD ftp://46.38.249.170/dist.sqlite > /home/pi/backups/`date -I`.sqlite
# release
- deploy (gulp build; delete .git; gulp buildcontrol:openshift --remote ssh://...;)
# app.js netcup
'use strict'; 
process.env.PORT = 80;
process.env.NODE_ENV = 'production';
require('./server/app.js');
