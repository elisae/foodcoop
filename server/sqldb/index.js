/**
 * Sequelize initialization module
 */

'use strict';

import config from '../config/environment';
import Sequelize from 'sequelize';

var db = {
  Sequelize,
  sequelize: new Sequelize(config.sequelize.uri, config.sequelize.options)
};

// Insert models below
db.Orderable = db.sequelize.import('../api/orderable/orderable.model');
db.Product = db.sequelize.import('../api/product/product.model');
db.User = db.sequelize.import('../api/user/user.model');
db.Setting = db.sequelize.import('../api/setting/setting.model');

db.Orderable.hasMany(db.Product, {as: 'Products'});
db.User.hasMany(db.Product, {as: 'Products'});
db.Product.belongsTo(db.Orderable);
db.Product.belongsTo(db.User);

var schedule = require('node-schedule');
schedule.scheduleJob('00 00 02 * * *', function() {
  db.sequelize.query('DELETE from Sessions; end transaction; vacuum;');
  console.log('Sessions purged.');
});

module.exports = db;
