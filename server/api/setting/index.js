'use strict';

var express = require('express');
var router = express.Router();

import {
  Setting
} from '../../sqldb';
import {
  respondWithResult,
  handleError,
  handleEntityNotFound,
  saveUpdates
} from '../helpers';

// Gets a list of Settings
export function index(req, res) {
  return Setting.findAll({
      where: {
        key: req.params.key
      }
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Setting from the DB
export function show(req, res) {
  return Setting.find({
      where: {
        _id: req.params.id
      }
    })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

function preSave(req) {
  var newEntity = req.body;
  return newEntity;
}

// Creates a new Setting in the DB
export function create(req, res) {
  return Setting.create(preSave(req))
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Setting in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Setting.find({
      where: {
        _id: req.params.id,
      }
    })
    .then(handleEntityNotFound(res))
    .then(saveUpdates(preSave(req)))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Setting from the DB
export function destroy(req, res) {
  return Setting.find({
      where: {
        _id: req.params.id
      }
    })
    .then(handleEntityNotFound(res))
    .then(saveUpdates({
      deleted: true
    }))
    .then(() => {
      res.status(204).end();
    })
    .catch(handleError(res));
}


router.get('/:key', index);
router.post('/', create);
router.put('/:id', update);
router.patch('/:id', update);
router.delete('/:id', destroy);

module.exports = router;
