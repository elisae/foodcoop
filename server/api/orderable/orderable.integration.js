'use strict';

var app = require('../..');
import request from 'supertest';

var newOrderable;

describe('Orderable API:', function () {
  describe('GET /api/orderables', function () {
    var orderables;

    beforeEach(function (done) {
      request(app)
        .get('/api/orderables/2012/12')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          orderables = res.body;
          done();
        });
    });

    it('should respond with JSON array', function () {
      orderables.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/orderables', function () {
    beforeEach(function (done) {
      request(app)
        .post('/api/orderables')
        .send({
          name: 'New Orderable'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newOrderable = res.body;
          done();
        });
    });

    it('should respond with the newly created orderable', () => {
      newOrderable.name.should.equal('New Orderable');
    });
  });

  describe('PUT /api/orderables/:id', function () {
    var updatedOrderable;

    beforeEach(function (done) {
      request(app)
        .put(`/api/orderables/${newOrderable._id}`)
        .send({
          name: 'Updated Orderable'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          updatedOrderable = res.body;
          done();
        });
    });

    afterEach(function () {
      updatedOrderable = {};
    });

    it('should respond with the updated orderable', function () {
      updatedOrderable.name.should.equal('Updated Orderable');
    });
  });

  describe('DELETE /api/orderables/:id', function () {
    it('should respond with 204 on successful removal', function (done) {
      request(app)
        .delete(`/api/orderables/${newOrderable._id}`)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when orderable does not exist', function (done) {
      request(app)
        .delete(`/api/orderables/${newOrderable._id}`)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
