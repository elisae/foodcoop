/**
 * Main application file
 */

'use strict';

import express from 'express';
import sqldb from './sqldb';
import config from './config/environment';
import http from 'http';
import helmet from 'helmet';

// Populate databases with sample data
if (config.seedDB) {
  require('./config/seed');
}

// Setup server
var app = express();
app._ready = new Promise((resolve, reject) => {
  var server = http.createServer(app);
  require('./config/express').default(app);
  require('./routes').default(app);

  app.use(helmet());

  // Start server
  function startServer() {
    app.angularFullstack = server.listen(config.port, config.ip, function () {
      console.log('Express server listening on %d, in %s mode', config.port,
        app.get('env'));
      resolve();
    });
  }

  sqldb.sequelize
    .sync()
    .then(startServer)
    .catch(function (err) {
      console.log('Server failed to start due to error: %s', err);
    });
});


// Expose app
exports = module.exports = app;
